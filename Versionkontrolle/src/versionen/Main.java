package versionen;
/*	
 * VERSIONSKONTROLLE
 * 
 * 	-> 	Konfliktfreier bearbeitung und Verwaltung von Dateien durch mehrere Instanzen
 * 		über die gesamte Enwicklungszeit, wobei jede Variante der Dateien als Historie gespeichert wird. 
 * 
 *  -> 	Diese Historie wird in einem sogenannten Repository gehalten (kurz : Repo)
 *  
 *  ->	Das Verschmelzen von Varianten einer Bestimmten Datei folgt in jedem Versionskontrollsystem
 *  	einem bestimmten Vertrag : Dem sog. Mergevertrag
 *  		-> Das Verschmelzen zu einer gemeinsamen, aktuellen Versions nennt man Merge
 *  
 *  -> Bei der gesamten Entwicklung gibt es immer einen Haupt-Entwicklungszweig/Hauptast
 *  		-> master
 *  
 *  ->	Nebenentwicklungsstränger nennt man "branch" / "branches"
 *  
 *  ->	Jeder entwicjkungsstrang hat immer eine Datei, welche für die aktuellste Variante steht, und diese wird 
 *  	"head" genannt 
 *  
 *  ->	Beim Merging wird es Konflikte gibt in den Dateiständen, der Merge-Vorgang
 *  	blockiertm bis der Konflikt von den Entwickler gelöst wurde 
 *  			-> 	Teamfoundation Server (Microsoft)
 *  			->	SVN 
 *  			-> 	Git (stamm von Linux entwicklung )
 *  
 *  
 *	->	Grundlegende Konzepte von Git : 
 *				->	Jede Datei in einem Repository existiert nur einmal 
 *				->	Git arbeitet hauptsächlich mit Hash-Werten
 *				->	Dateien werden als Schlüssel angelegt (seg. "Blobs")
 *					-> Blobs sind auf Hashing besierende Dateien, welche Unterschiede zur Original -Datei speichern
 *				-> Sowohl Dateien als auch die zugehörigen Blobs werden baumartig intern verwaltet
 *
 *  
 * 			->	zunächst wird "nur" sog. "Workign directory" gearbeitet!!
 * 				->	Änderungen, die hier stattfinden, werden NICHT sofort /automatisch 
 * 					ins lokale Repository abgelegt.
 * 
 * 			->	der Befehl COMMIT: 
 * 						- 	Durchgeführte Änderungen werden ins Repository übernommen. 
 *  					
 *  					- 	Jeder Commit im Prinzip ein "Zeiger" auf den jeweiligen branch, auf welchen sich
 *  						dieser Commit bezieht.
 *  
 *  	->	Wenn man mit Git entwickelt, wird normalerweise immer vom Hauptzweig (master) abgezweigt in einem neuen Branch
 *  
 *  		-> 	In diesem neuen Branch wird solange entwickeltm bis eine Version vorliegen welche nach Ansicht des Entwicklers "verschmolzen" werden, 
 *  			kann mit dem lokalen Hauptzweig (master)
 *  
 *  
 *  	-> 	der Befehl, um zwischen branches zu wechseln, heißt CHECKOUT
 *  
 *   	->	Normalerweise wird regelmässig (nachdem abgezweigte branches lokal wieder zusammengeführt werden) per PUSH 
 *   		die aktualle Version in Remote-Repo geladen. 
 *   
 *   	
 *  
 * 
 * 
 * 
 * 				
 * 
 * 
 */

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
